<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PretRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     collectionOperations={
            "get" = {
 *              "method" = "GET",
 *              "path" = "/prets",
 *              "access_control" = "is_granted('ROLE_MANAGER')",
 *              "access_control_message" = "Vous n'avez pas les droits d'accès à cette ressource."
 *          },
 *          "post" = {
 *              "method" = "POST",
 *              "path" = "/prets",
 *              "denormalization_context" = {
                    "groups" = {"pret_post_adherent"}
 *              }
 *          }
 *     },
 *     itemOperations={
            "get" = {
 *              "method" = "GET",
 *              "path" = "/prets/{id}",
 *              "access_control" = "(is_granted('ROLE_ADHERENT') and object.getAdherent() == user) or is_granted('ROLE_MANAGER')",
 *              "access_control_message" = "Vous ne pouvez avoir accès qu'à vos propres prêts."
 *          },
 *          "put" = {
 *              "method" = "PUT",
 *              "path" = "/manager/prets/{id}",
 *              "access_control" = "is_granted('ROLE_MANAGER')",
 *              "access_control_message" = "Vous n'avez pas les droits d'accès à cette ressource",
 *              "denormalization_context" = {
                    "groups" = {"put_manager"}
 *              }
 *          },
 *          "delete" = {
 *              "method" = "DELETE",
 *              "path" = "/prets/{id}",
 *              "access_control" = "is_granted('ROLE_MANAGER')",
 *              "access_control_message" = "Vous n'avez pas les droits d'accès à cette ressource"
 *          },
 *     }
 * )
 */
class Pret
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePret;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRetourPrevue;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"put_manager"})
     */
    private $dateRetourReelle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Livre", inversedBy="prets", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"pret_post_adherent"})
     */
    private $livre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Adherent", inversedBy="prets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adherent;

    public function __construct()
    {
        $this->datePret = new \DateTime();
        //doit obligatoirement être transformée en TimeStamp pour effectuer des calculs, puis en chaîne de caractère
        $dateRetourPrevue = date('Y-m-d H:m:n', strtotime('15 days', $this->getDatePret()->getTimestamp()));
        // et enfin en objet DateTime pour être stocké en BDD
        $this->dateRetourPrevue = \DateTime::createFromFormat('Y-m-d H:m:n', $dateRetourPrevue);
        $this->dateRetourReelle = null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatePret(): ?\DateTimeInterface
    {
        return $this->datePret;
    }

    public function setDatePret(\DateTimeInterface $datePret): self
    {
        $this->datePret = $datePret;

        return $this;
    }

    public function getDateRetourPrevue(): ?\DateTimeInterface
    {
        return $this->dateRetourPrevue;
    }

    public function setDateRetourPrevue(\DateTimeInterface $dateRetourPrevue): self
    {
        $this->dateRetourPrevue = $dateRetourPrevue;

        return $this;
    }

    public function getDateRetourReelle(): ?\DateTimeInterface
    {
        return $this->dateRetourReelle;
    }

    public function setDateRetourReelle(?\DateTimeInterface $dateRetourReelle): self
    {
        $this->dateRetourReelle = $dateRetourReelle;

        return $this;
    }

    public function getLivre(): ?Livre
    {
        return $this->livre;
    }

    public function setLivre(?Livre $livre): self
    {
        $this->livre = $livre;

        return $this;
    }

    public function getAdherent(): ?Adherent
    {
        return $this->adherent;
    }

    public function setAdherent(?Adherent $adherent): self
    {
        $this->adherent = $adherent;

        return $this;
    }
}
