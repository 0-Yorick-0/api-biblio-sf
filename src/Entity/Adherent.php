<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdherentRepository")
 * @UniqueEntity(
 *     fields={"mail"},
 *     message="il existe déjà un mail {{ value }}, veuillez saisir un autre mail"
 * )
 * @ApiResource(
 *      collectionOperations={
            "get" = {
 *              "method" = "GET",
 *              "path" = "/adherents",
 *              "access_control" = "is_granted('ROLE_MANAGER')",
 *              "access_control_message" = "Vous n'avez pas les droits d'accès à cette ressource."
 *          },
 *          "post" = {
 *              "method" = "POST",
 *              "path" = "/adherents",
 *              "access_control" = "is_granted('ROLE_MANAGER')",
 *              "access_control_message" = "Vous n'avez pas les droits d'accès à cette ressource.",
 *              "denormalization_context" = {
                    "groups" = {"adherent_post_role_manager"}
 *              }
 *          },
 *          "statNbPretsParAdherent" = {
                "method" = "GET",
 *              "route_name" = "adherents_nbPrets",
 *              "controller" = StatsController::class
 *          }
 *     },
 *     itemOperations={
            "get" = {
 *              "method" = "GET",
 *              "path" = "/adherents/{id}",
 *              "access_control" = "(is_granted('ROLE_ADHERENT') and object == user) or is_granted('ROLE_MANAGER')",
 *              "access_control_message" = "Vous n'avez pas les droits d'accès à cette ressource.",
 *              "normalization_context"={
                    "groups" = {"get_role_adherent"}
 *              }
 *          },
 *          "getNbPrets"={
                "method"="GET",
 *              "route_name"="adherent_prets_count"
 *          },
 *          "put" = {
 *              "method" = "PUT",
 *              "path" = "/adherents/{id}",
 *              "access_control" = "(is_granted('ROLE_ADHERENT') and object == user) or is_granted('ROLE_MANAGER')",
 *              "access_control_message" = "Vous n'avez pas les droits d'accès à cette ressource",
 *              "denormalization_context" = {
                    "groups" = {"get_role_adherent"}
 *              }
 *          },
 *          "delete" = {
 *              "method" = "DELETE",
 *              "path" = "/adherents/{id}",
 *              "access_control" = "is_granted('ROLE_ADMIN')",
 *              "access_control_message" = "Vous n'avez pas les droits d'accès à cette ressource"
 *          },
 *     }
 * )
 */
class Adherent implements UserInterface
{
    const ROLE_ADMIN = "ROLE_ADMIN";
    const ROLE_MANAGER = "ROLE_MANAGER";
    const ROLE_ADHERENT = "ROLE_ADHERENT";
    const DEFAULT_ROLE = "ROLE_ADHERENT";
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"adherent_post_role_manager", "get_role_adherent", "put_role_manager", "adherent_put_role_admin"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"adherent_post_role_manager", "get_role_adherent", "put_role_manager","adherent_put_role_admin"})
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"adherent_post_role_manager", "get_role_adherent", "put_role_manager", "adherent_put_role_admin"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"adherent_post_role_manager", "get_role_adherent", "put_role_manager", "adherent_put_role_admin"})
     */
    private $codeCommune;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"adherent_post_role_manager", "adherent_put_role_admin"})
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"adherent_post_role_manager", "get_role_adherent", "put_role_manager", "adherent_put_role_admin"})
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"adherent_post_role_manager", "adherent_put_role_admin"})
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pret", mappedBy="adherent")
     * @Groups({"adherent_post_role_manager", "get_role_adherent", "put_role_manager", "adherent_put_role_admin"})
     * @ApiSubresource()
     */
    private $prets;

    /**
     * @ORM\Column(type="array", length=255, nullable=true)
     * @Groups({"adherent_post_role_admin", "adherent_put_role_admin"})
     */
    private $roles;

    public function __construct()
    {
        $this->prets = new ArrayCollection();
        $role[] = self::DEFAULT_ROLE;
        $this->roles = $role;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodeCommune(): ?string
    {
        return $this->codeCommune;
    }

    public function setCodeCommune(?string $codeCommune): self
    {
        $this->codeCommune = $codeCommune;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Pret[]
     */
    public function getPrets(): Collection
    {
        return $this->prets;
    }

    public function addPret(Pret $pret): self
    {
        if (!$this->prets->contains($pret)) {
            $this->prets[] = $pret;
            $pret->setAdherent($this);
        }

        return $this;
    }

    public function removePret(Pret $pret): self
    {
        if ($this->prets->contains($pret)) {
            $this->prets->removeElement($pret);
            // set the owning side to null (unless already changed)
            if ($pret->getAdherent() === $this) {
                $pret->setAdherent(null);
            }
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * affecte les rôles de l'utilisateur
     * @param mixed $roles
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getUsername(): ?string
    {
        return $this->getMail();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
