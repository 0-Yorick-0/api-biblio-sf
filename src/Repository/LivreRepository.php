<?php

namespace App\Repository;

use App\Entity\Livre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Livre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Livre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Livre[]    findAll()
 * @method Livre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LivreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Livre::class);
    }

public function meilleursLivres()
{
    $query = $this->createQueryBuilder('l')
        //'l' pour tous les champs de la classe Livre
        ->select('l as livre, count(p.id) as nbprets')
        ->join('l.prets', 'p')
        ->groupBy('l')
        ->orderBy('nbprets', 'DESC')
        ->setMaxResults(5);
    return $query->getQuery()->getResult();
}
}
