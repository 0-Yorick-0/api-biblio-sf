<?php


namespace App\Services;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Pret;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PretSubscriber implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $token;

    public function __construct(TokenStorageInterface $token)
    {
        $this->token = $token;
    }

    public static function getSubscribedEvents()
    {
        return [
            //à chaque évenement déclenché, on regarde si on est en train d'écrire en BDD (typiquement lors d'un flush).
            // si c'est le cas, on lance la méthode getAuthenticatedUser()
            KernelEvents::VIEW => ['getAuthenticatedUser', EventPriorities::PRE_WRITE]
        ];
    }

    public function getAuthenticatedUser(ViewEvent $event)
    {
        $entity = $event->getControllerResult(); //récupération de l'entité qui a déclenché l'évenement
        $method = $event->getRequest()->getMethod(); //récupère la méthode invoquée dans la requête
        $adherent = $this->token->getToken()->getUser(); //récupère l'adhérent actuellement connecté

        //s'il s'agit bien d'un prêt en POST alors on lui ajoute l'adhérent connecté
        if ($entity instanceof Pret && $method == Request::METHOD_POST) {
            $entity->setAdherent($adherent);
        }
        return;
    }
}