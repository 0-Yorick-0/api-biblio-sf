<?php


namespace App\Services;


use App\Entity\Livre;
use App\Entity\Pret;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;

class DoctrinePretSubscriber implements EventSubscriber
{

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            //Events::preUpdate,
            //Events::preRemove,
            Events::onFlush
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $pret = $args->getObject();
        if ($pret instanceof Pret) {
            $pret->getLivre()->setDispo(false);
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $pret = $args->getObject();
        if ($pret instanceof Pret) {
            if ($pret->getDateRetourReelle() == null){
                $pret->getLivre()->setDispo(false);
            }else {
                $pret->getLivre()->setDispo(true);
            }
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $pret = $args->getObject();
        if ($pret instanceof Pret) {
            $pret->getLivre()->setDispo(true);
        }
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Pret) {
                $livre = $entity->getLivre();
                if ($entity->getDateRetourReelle() == null){
                    $livre->setDispo(false);
                }else {
                    $livre->setDispo(true);
                }
                //absolument nécessaire pour valider les opérations ci-dessus en base
                //cf https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/events.html#onflush
                $metaData = $em->getClassMetadata(Livre::class);
                $uow->recomputeSingleEntityChangeSet($metaData, $livre);
            }
        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if ($entity instanceof Pret) {
                $livre = $entity->getLivre();
                $livre->setDispo(true);
                $metaData = $em->getClassMetadata(Livre::class);
                $uow->recomputeSingleEntityChangeSet($metaData, $livre);
            }
        }
    }
}