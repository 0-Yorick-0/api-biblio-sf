<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Repository\GenreRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiGenreController extends AbstractController
{
    /**
     * @Route("/api/genres", name="api_genres", methods={"GET"})
     */
    public function list(GenreRepository $repository, SerializerInterface $serializer)
    {
        $genres = $repository->findAll();
        //le tableau d'index 'groups' indique les attributs de l'entité qui doivent être ciblés pour la sérialisation
        $genres = $serializer->serialize($genres, 'json', ['groups' => ['listGenreFull']]);
        //le booleen précise que la data est déjà en JSON
        return new JsonResponse($genres, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/genres/{id}", name="api_genres_show", methods={"GET"})
     */
    public function show(Genre $genre, SerializerInterface $serializer)
    {
        //le tableau d'index 'groups' indique les attributs de l'entité qui doivent être ciblés pour la sérialisation
        $genre = $serializer->serialize($genre, 'json', ['groups' => ['listGenreSimple']]);
        //le booleen précise que la data est déjà en JSON
        return new JsonResponse($genre, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/genres", name="api_genres_creates", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer, ValidatorInterface $validator, EntityManagerInterface $manager)
    {
        $data = $request->getContent();
        //désérialise les info de la requête pour en faire un objet de type Genre
        $genre = $serializer->deserialize($data, Genre::class, 'json');
        //gestion des erreurs de validation
        $errors = $validator->validate($genre);
        if (count($errors)) {
            $errorsJson = $serializer->serialize($errors, 'json');
            return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
        }
        $manager->persist($genre);
        $manager->flush();

        //le booleen précise que la data est déjà en JSON
        return new JsonResponse(null, Response::HTTP_OK, [
            'location' => 'api/genres/' . $genre->getId()
        ]);
    }

    /**
     * @Route("/api/genres/{id}", name="api_genres_update", methods={"PUT"})
     */
    public function edit(Genre $genre, SerializerInterface $serializer, Request $request, ValidatorInterface $validator, EntityManagerInterface $manager)
    {
        $data = $request->getContent();
        //ici on modifie un objet qui existe déjà, d'où le dernier paramètre qui indique le contexte
        $genre = $serializer->deserialize($data, Genre::class, 'json', ['object_to_populate' => $genre]);

        //gestion des erreurs de validation
        $errors = $validator->validate($genre);
        if (count($errors)) {
            $errorsJson = $serializer->serialize($errors, 'json');
            return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
        }

        $manager->persist($genre);
        $manager->flush();

        return new JsonResponse(null, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/genres/{id}", name="api_genres_delete", methods={"DELETE"})
     */
    public function delete(Genre $genre, EntityManagerInterface $manager)
    {
        $manager->remove($genre);
        $manager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT, []);
    }
}
