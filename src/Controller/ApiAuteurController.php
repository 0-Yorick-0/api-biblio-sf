<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Repository\AuteurRepository;
use App\Repository\NationaliteRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiAuteurController extends AbstractController
{
    /**
     * @Route("/api/auteurs", name="api_auteurs", methods={"GET"})
     */
    public function list(AuteurRepository $repository, SerializerInterface $serializer)
    {
        $auteurs = $repository->findAll();
        //le tableau d'index 'groups' indique les attributs de l'entité qui doivent être ciblés pour la sérialisation
        $auteurs = $serializer->serialize($auteurs, 'json', ['groups' => ['listAuteurFull']]);
        //le booleen précise que la data est déjà en JSON
        return new JsonResponse($auteurs, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/auteurs/{id}", name="api_auteurs_show", methods={"GET"})
     */
    public function show(Auteur $auteur, SerializerInterface $serializer)
    {
        //le tableau d'index 'groups' indique les attributs de l'entité qui doivent être ciblés pour la sérialisation
        $auteur = $serializer->serialize($auteur, 'json', ['groups' => ['listAuteurSimple']]);
        //le booleen précise que la data est déjà en JSON
        return new JsonResponse($auteur, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/auteurs", name="api_auteurs_creates", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer, ValidatorInterface $validator, NationaliteRepository $nationaliteRepository, EntityManagerInterface $manager)
    {
        $data = $request->getContent();
        //on récupère d'abord la data sous forme de tableau, afin d'extraire l'id de la nationalité. La problématique est de vérifier l'existence de la nationalité en base,
        // afin d'empêcher Doctrine de créer automatiquement n'importe quoi dans la table `nationalité`
        $dataTab = $serializer->decode($data, 'json');
        $nationalite = $nationaliteRepository->find($dataTab['nationalite']['id']);
        //désérialise les info de la requête pour en faire un objet de type Auteur
        $auteur = $serializer->deserialize($data, Auteur::class, 'json');
        $auteur->setNationalite($nationalite);
        //gestion des erreurs de validation
        $errors = $validator->validate($auteur);
        if (count($errors)) {
            $errorsJson = $serializer->serialize($errors, 'json');
            return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
        }
        $manager->persist($auteur);
        $manager->flush();

        //le booleen précise que la data est déjà en JSON
        return new JsonResponse(null, Response::HTTP_OK, [
            'location' => 'api/auteurs/' . $auteur->getId()
        ]);
    }

    /**
     * @Route("/api/auteurs/{id}", name="api_auteurs_update", methods={"PUT"})
     */
    public function edit(Auteur $auteur, SerializerInterface $serializer, Request $request, ValidatorInterface $validator, NationaliteRepository $nationaliteRepository, EntityManagerInterface $manager)
    {
        $data = $request->getContent();
        //on récupère d'abord la data sous forme de tableau, afin d'extraire l'id de la nationalité. La problématique est de vérifier l'existence de la nationalité en base,
        // afin d'empêcher Doctrine de créer automatiquement n'importe quoi dans la table `nationalité`
        $dataTab = $serializer->decode($data, 'json');
        $nationalite = $nationaliteRepository->find($dataTab['nationalite']['id']);
        //ici on modifie un objet qui existe déjà, d'où le dernier paramètre qui indique le contexte
        $auteur = $serializer->deserialize($data, Auteur::class, 'json', ['object_to_populate' => $auteur]);
        $auteur->setNationalite($nationalite);
        //gestion des erreurs de validation
        $errors = $validator->validate($auteur);
        if (count($errors)) {
            $errorsJson = $serializer->serialize($errors, 'json');
            return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
        }

        $manager->persist($auteur);
        $manager->flush();

        return new JsonResponse('Auteur modifié', Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/auteurs/{id}", name="api_auteurs_delete", methods={"DELETE"})
     */
    public function delete(Auteur $auteur, EntityManagerInterface $manager)
    {
        $manager->remove($auteur);
        $manager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT, []);
    }
}
