<?php

namespace App\Controller;

use App\Repository\AdherentRepository;
use App\Repository\LivreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StatsController extends AbstractController
{
    /**
     * renvoie le nombre de prêts par adhérent
     * @Route(
     *     path="apiPlatform/adherent/nbPretsParAdherent",
     *     name="adherents_nbPrets",
     *     methods={"GET"}
     * )
     */
    public function nombrePretsParAdherent(AdherentRepository $repository)
    {
        $nbPretParAdherent = $repository->nbPretsParAdherent();
        return $this->json($nbPretParAdherent);
    }

    /**
     * renvoie les 5 livres les plus demandés
     * @Route(
     *     path="apiPlatform/livres/meilleurslivres",
     *     name="meilleurslivres",
     *     methods={"GET"}
     * )
     */
    public function meilleursLivres(LivreRepository $repository)
    {
        $meilleursLivres = $repository->meilleursLivres();
        return $this->json($meilleursLivres);
    }
}
