<?php

namespace App\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use App\Entity\Livre;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final class LivreContextBuilder implements SerializerContextBuilderInterface
{
    private $decorated;
    private $authorizationChecker;

    public function __construct(SerializerContextBuilderInterface $decorated, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->decorated = $decorated;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $resourceClass = $context['resource_class'] ?? null;

        //si la classe concernée est bien Livre, et si une annotation 'groups' existe, et si j'ai le droit 'ROLE_MANAGER' et qu'on est bien en normalisation
        // on pourrait aussi tester $request->getMethod() si besoin
        if ($resourceClass === Livre::class && isset($context['groups']) && $this->authorizationChecker->isGranted('ROLE_MANAGER') && true === $normalization) {
            //ajout du contexte de normalisation du manager
            $context['groups'][] = 'get_role_manager';
        }

        if ($resourceClass === Livre::class && isset($context['groups']) && $this->authorizationChecker->isGranted('ROLE_ADMIN') && false === $normalization) {
            //On s'assure qu'on n'est pas dans un POST
            if ($request->getMethod() == 'PUT') {
                //ajout du contexte de normalisation de l'admin
                $context['groups'][] = 'put_admin';
            }
        }

        return $context;
    }
}