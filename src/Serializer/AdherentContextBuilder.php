<?php

namespace App\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use App\Entity\Adherent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final class AdherentContextBuilder implements SerializerContextBuilderInterface
{
    private $decorated;
    private $authorizationChecker;

    public function __construct(SerializerContextBuilderInterface $decorated, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->decorated = $decorated;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $resourceClass = $context['resource_class'] ?? null;

        if ($resourceClass === Adherent::class && isset($context['groups']) && $this->authorizationChecker->isGranted('ROLE_MANAGER') && false === $normalization) {
            //On s'assure qu'on n'est pas dans un POST
            if ($request->getMethod() == 'PUT') {
                //ajout du contexte de normalisation du manager
                $context['groups'][] = 'put_role_manager';
            }
        }

        if ($resourceClass === Adherent::class && isset($context['groups']) && $this->authorizationChecker->isGranted('ROLE_ADMIN') && false === $normalization) {
            //On s'assure qu'on est dans un POST
            if ($request->getMethod() == 'POST') {
                //ajout du contexte de normalisation du manager
                $context['groups'][] = 'adherent_post_role_admin';
            }
        }

        if ($resourceClass === Adherent::class && isset($context['groups']) && $this->authorizationChecker->isGranted('ROLE_ADMIN') && false === $normalization) {
            if ($request->getMethod() == 'PUT') {
                //ajout du contexte de normalisation du manager
                $context['groups'][] = 'adherent_put_role_admin';
            }
        }

        return $context;
    }
}